/**
 * A locus is a focus range along a dimension from -1 to 1
 * with continually increasing precision the more address
 * segments are provided
 *
 * usage: locus(Uint8ClampedArray.from[0b10000000])
 */

import { Uint8Bits } from './Uint8Bits.js'

/**
 * address is by 8bit group:
 *  significant sign i.e. 00 // do nothing by 1
 *  significant sign i.e. 01 // do nothing by 1/2
 *  significant sign i.e. 10 // move negative by 1/4
 *  significant sign i.e. 11 // move positive by 1/8
 *  -> final value is -1/8
 * each successive 8bit group has 1/2 the value of the previous
 * @param {Uint8ClampedArray} address
 */
export const locus = (address = Uint8ClampedArray.from([])) => ({
  get address () {
    return address
  },

  get sign () {
    if (address.length > 0) {
      return Boolean(address[0] & 0x80)
    }
  },

  /**
   * converts to an integer at a resolution, with range -resolution to resolution
   * i.e. locus.value(100) // -100 ... 100
   * @param {number} resolution
   */
  value (resolution) {
    if (isNaN(resolution) || resolution <= 0 || resolution > Number.MAX_SAFE_INTEGER) {
      throw new RangeError('resolution is not in range 0 < resolution < MAX_SAFE_INTEGER')
    }
    const internalResolution = Math.pow(2, Math.ceil(Math.log2(resolution)))
    const ratio = resolution / internalResolution
    let result = 0 // the value
    let range = 0 // possible range in output value
    let sign = 0 // 0 = negative, 1 = positive
    for (const [index, entry] of address.entries()) {
      // index in 0 - ∞
      // entry in 0 - 255, 0x00 - 0xff, 0b00000000 - 0b11111111
      for (const [position, mask] of Uint8Bits.entries()) {
        // position in 0 - 7, 0b000 - 0b111
        // mask in 0b10000000 .. 0b00000001
        if (index === 0 && position === 0) {
          sign = entry & mask
          result = sign ? internalResolution : -internalResolution
          continue
        }
        const cursor = index + position // 1, 2, 3, ...
        range = range + Math.pow(2, cursor) // we're counting backwards, but the result is the same
        // debug console.log({ result, range, internalResolution, entry, mask })
        if (range > internalResolution) { // continuing would not further refine the result
          return result > 0 ? Math.floor(ratio * result) : Math.ceil(ratio * result)
        }
        if (entry & mask) {
          // debug console.log('Adding Math.pow( 2, -', cursor, ') *', (sign ? -1 : 1) * internalResolution)
          result += (sign ? -1 : 1) * internalResolution * Math.pow(2, -cursor)
        }
      }
    }
    return result > 0 ? Math.floor(ratio * result) : Math.ceil(ratio * result)
  },

  /**
   * @param {number} depth
   */
  size (depth) { },

  /**
   * @param {number} depth
   */
  leftBound (depth) { },

  /**
   * @param {number} depth
   */
  rightBound (depth) { },

  /**
   * @param {Uint8ClampedArray} address
   * @param {number} depth
   */
  distance (address, depth) { }
})
