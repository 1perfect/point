import { strictEqual } from 'assert'

import { locus } from './locus.js'

import {
  Uint8OrderedByFirstBit,
  Uint8AllValues
} from './Uint8Bits.js'

const testLengths = [0, 1] // careful, adding a 2 in here causes over 16 million tests to run

const addressesByLength = testLengths.map(length => {
  const addresses = []
  if (length > 0) {
    for (const value1 of Uint8OrderedByFirstBit) {
      if (length === 1) {
        addresses.push(Uint8ClampedArray.from([value1]))
        continue
      } else {
        for (const value2 of Uint8AllValues) {
          if (length === 2) {
            addresses.push(Uint8ClampedArray.from([value1, value2]))
            continue
          } else {
            throw new RangeError('length must be in 0..3 because commodity hardware')
          }
        }
      }
    }
  }
  return addresses
})

const expectedlocusValuesByLength = [
  [],
  [
    255,
    127,
    191,
    63,
    223,
    95,
    159,
    31,
    239,
    111,
    175,
    47,
    207,
    79,
    143,
    15,
    247,
    119,
    183,
    55,
    215,
    87,
    151,
    23,
    231,
    103,
    167,
    39,
    199,
    71,
    135,
    7,
    251,
    123,
    187,
    59,
    219,
    91,
    155,
    27,
    235,
    107,
    171,
    43,
    203,
    75,
    139,
    11,
    243,
    115,
    179,
    51,
    211,
    83,
    147,
    19,
    227,
    99,
    163,
    35,
    195,
    67,
    131,
    3,
    253,
    125,
    189,
    61,
    221,
    93,
    157,
    29,
    237,
    109,
    173,
    45,
    205,
    77,
    141,
    13,
    245,
    117,
    181,
    53,
    213,
    85,
    149,
    21,
    229,
    101,
    165,
    37,
    197,
    69,
    133,
    5,
    249,
    121,
    185,
    57,
    217,
    89,
    153,
    25,
    233,
    105,
    169,
    41,
    201,
    73,
    137,
    9,
    241,
    113,
    177,
    49,
    209,
    81,
    145,
    17,
    225,
    97,
    161,
    33,
    193,
    65,
    129,
    -255,
    -127,
    -191,
    -63,
    -223,
    -95,
    -159,
    -31,
    -239,
    -111,
    -175,
    -47,
    -207,
    -79,
    -143,
    -15,
    -247,
    -119,
    -183,
    -55,
    -215,
    -87,
    -151,
    -23,
    -231,
    -103,
    -167,
    -39,
    -199,
    -71,
    -135,
    -7,
    -251,
    -123,
    -187,
    -59,
    -219,
    -91,
    -155,
    -27,
    -235,
    -107,
    -171,
    -43,
    -203,
    -75,
    -139,
    -11,
    -243,
    -115,
    -179,
    -51,
    -211,
    -83,
    -147,
    -19,
    -227,
    -99,
    -163,
    -35,
    -195,
    -67,
    -131,
    -3,
    -253,
    -125,
    -189,
    -61,
    -221,
    -93,
    -157,
    -29,
    -237,
    -109,
    -173,
    -45,
    -205,
    -77,
    -141,
    -13,
    -245,
    -117,
    -181,
    -53,
    -213,
    -85,
    -149,
    -21,
    -229,
    -101,
    -165,
    -37,
    -197,
    -69,
    -133,
    -5,
    -249,
    -121,
    -185,
    -57,
    -217,
    -89,
    -153,
    -25,
    -233,
    -105,
    -169,
    -41,
    -201,
    -73,
    -137,
    -9,
    -241,
    -113,
    -177,
    -49,
    -209,
    -81,
    -145,
    -17,
    -225,
    -97,
    -161,
    -33,
    -193,
    -65,
    -129,
    -1
  ]
]

console.log('Test locus value, start')

testLengths.forEach(testLength => {
  addressesByLength[testLength].forEach((address, index) => {
    const locus1 = locus(address)
    strictEqual(locus1.value(0xff), expectedlocusValuesByLength[testLength][index])
  })

  const expectedIncreasingResValuesByLength = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [92, 184, 276, 368, 460, 553, 645, 737]
  ]
  expectedIncreasingResValuesByLength[testLength].map((expected, index) => {
    const address = addressesByLength[testLength][testLength === 1 ? 40 : 0]
    const increasingRes = locus(address)
    strictEqual(increasingRes.value((index + 1) * 100), expected)
  })
})

console.log('All tests pass!')
