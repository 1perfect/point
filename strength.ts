type StrengthEntry = [Uint8ClampedArray, Uint8ClampedArray]

interface Strength {
  addressTable: StrengthEntry[]
}

export const strength: (addressTable: StrengthEntry[]) => Strength
