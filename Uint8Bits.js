export const Uint8Bits = Uint8ClampedArray.from([
  0b10000000, // 0x80
  0b01000000, // 0x40
  0b00100000, // 0x20
  0b00010000, // 0x10
  0b00001000, // 0x8
  0b00000100, // 0x4
  0b00000010, // 0x2
  0b00000001 //  0x1
])
/**
 * Contains all possible Uint8 values
 * 0b00000000 0x00
 * 0b10000000 0x80
 * 0b01000000 0x40
 * 0b11000000 0xc0
 * ...
 * 0b11111111 0xff
 *
 * bit positions are as follows
 * 0b10000000 0x80
 * 0b01000000 0x40
 * 0b00100000 0x20
 * 0b00010000 0x10
 * 0b00001000 0x8
 * 0b00000100 0x4
 * 0b00000010 0x2
 * 0b00000001 0x1
 */
export const Uint8AllValues = Array(0xff)
  .fill(null)
  .map((_, index) => (
    (index & 0x80 ? 0x1 : 0) |
    (index & 0x40 ? 0x2 : 0) |
    (index & 0x20 ? 0x4 : 0) |
    (index & 0x10 ? 0x8 : 0) |
    (index & 0x8 ? 0x10 : 0) |
    (index & 0x4 ? 0x20 : 0) |
    (index & 0x2 ? 0x40 : 0) |
    (index & 0x1 ? 0x80 : 0)))

export const Uint8WithFirstBit0 = Uint8AllValues.filter(x => !(x & 0x80))
export const Uint8WithFirstBit1 = Uint8AllValues.filter(x => x & 0x80)
export const Uint8OrderedByFirstBit = Uint8WithFirstBit1.concat(Uint8WithFirstBit0)
