interface Locus {
  address: Uint8ClampedArray
  sign: boolean | undefined
  value(resolution: number): number
  size(depth: number): Uint8ClampedArray
  minBound(depth: number): Uint8ClampedArray
  maxBound(depth: number): Uint8ClampedArray
  distance(address: Uint8ClampedArray, depth: number): Uint8ClampedArray
}

export const locus: (address: Uint8ClampedArray) => Locus
