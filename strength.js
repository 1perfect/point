/**
 * A strength defines intensity of a value over a variable domain
 * density along a dimension from -1 to 1 created by chaining points
 * with continually increasing precision the more addresses
 * are provided to each point
 *
 * usage: strength([
 *   Uint8ClampedArray.from[0b10000000],
 *   Uint8ClampedArray.from[0b10000000]
 * ])
 */

/**
 * @param {Array<[Uint8ClampedArray, Uint8ClampedArray]>} addressTable
 */
export const strength = (addressTable) => ({

})
